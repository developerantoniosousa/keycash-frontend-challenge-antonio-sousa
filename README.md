### Screenshots

<div style="display: block">
  <p>No Android</p>
  <img width="200px" src=".readme/android-properties-list.png" alt="Lista de imóveis no Android" title="Lista de imóveis no Android">
  <p>No iOS</p>
  <img width="200px" src=".readme/ios-properties-list.png" alt="Lista de imóveis no iOS" title="Lista de imóveis no iOS">
</div>
<p>Observação: para carregar as imagens acima é necessário abrir o arquivo através de um leitor de MD que tenha suporte a mostrar imagens</p>

### Como rodar localmente?

Para rodar o projeto localmente é necessário possui um ambiente de desenvolvimento configurado conforme instruções do [guia](https://reactnative.dev/docs/environment-setup).

Com o ambiente de desenvolvimento corretamente configurado. Acesse a pasta do projeto e execute um dos comandos a seguir dependendo da plataforma desejada:

```
// para rodar no iOS
$ yarn ios
ou
$ npm run ios

// para rodar no Android
$ yarn android
ou
$ npm run android
```

### Como gerar o APK?

Para gerar o apk, acesse a pasta do projeto e execute o comando abaixo no terminal.

```
$ cd android && ./gradlew assembleRelease
```

O arquivo APK é salvo no caminho <nome_projeto>/android/app/build/outputs/apk/release

### Como fazer o deploy?

Para realizar o deploy para a Play Store é necessário gerar e enviar um arquivo de extensão AAB (Android App Bundle). Para gerar a compilação para distribuição execute o seguinte comando na pasta do projeto:

```
$ cd android && ./gradlew bundleRelease
```

O arquivo AAB é salvo no caminho <nome_projeto>/android/app/build/outputs/bundle/release
