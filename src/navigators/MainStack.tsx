import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {PropertyList} from 'scenes/PropertyList';
import {PropertyDetails} from 'scenes/PropertyDetails';

const Stack = createNativeStackNavigator();

export function MainStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="PropertyList"
          component={PropertyList}
          options={PropertyList.navigationOptions}
        />
        <Stack.Screen
          name="PropertyDetails"
          component={PropertyDetails}
          options={PropertyDetails.navigationOptions}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
