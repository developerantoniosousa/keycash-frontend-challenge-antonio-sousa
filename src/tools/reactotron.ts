import Reactotron from 'reactotron-react-native';
import {reactotronRedux} from 'reactotron-redux';
import {name as appName} from '../../app.json';

if (__DEV__) {
  console.tron = Reactotron.configure({
    name: appName,
  })
    .useReactNative()
    .use(reactotronRedux())
    .connect();
}
