import {StoreEnhancer} from 'redux';
interface Reactotron {
  reduxStore?: any;
  /**
   * Enhancer creator
   */
  createEnhancer: (skipSettingStore?: boolean) => StoreEnhancer;
  /**
   * Store setter
   */
  setReduxStore?: (store: any) => void;
}
declare global {
  interface Console {
    tron: Reactotron;
  }
}
