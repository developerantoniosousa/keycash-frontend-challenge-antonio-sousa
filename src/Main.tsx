import './tools/reactotron';
import React from 'react';
import {Provider} from 'react-redux';
import {GlobalizeProvider} from 'react-native-globalize';
import {store} from './global/store';

import {MainStack} from 'navigators/MainStack';
import {LOCALE, CURRENCY_CODE} from 'locales/pt';

function App() {
  return (
    <Provider store={store}>
      <GlobalizeProvider locale={LOCALE} currency={CURRENCY_CODE}>
        <MainStack />
      </GlobalizeProvider>
    </Provider>
  );
}

export default App;
