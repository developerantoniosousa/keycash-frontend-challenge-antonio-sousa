import {useMemo} from 'react';
import {useGlobalize} from 'react-native-globalize';

import {CURRENCY_CODE} from 'locales/pt';

export default function useBrazilianCurrency(value: number) {
  const {formatCurrency} = useGlobalize();

  return useMemo(
    () => formatCurrency(value, CURRENCY_CODE),
    [formatCurrency, value],
  );
}
