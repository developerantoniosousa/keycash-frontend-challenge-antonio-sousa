import axios from 'axios';
import Env from './env';

const api = axios.create({
  baseURL: Env.API_URL,
  timeout: 5000,
});

export default api;
