import styled from 'styled-components/native';
import metrics from 'styles/metrics';

export const Container = styled.View`
  flex: 1;
  padding: ${metrics.padding}px;
`;
