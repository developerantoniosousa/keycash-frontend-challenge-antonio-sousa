import styled from 'styled-components/native';
import {fonts, colors} from 'styles';

export const ButtonText = styled.Text`
  color: ${colors.white};
  font-weight: bold;
  font-size: ${fonts.size.medium}px;
  text-align: center;
`;
