import styled from 'styled-components/native';
import {colors, metrics} from 'styles';

export const Button = styled.TouchableOpacity.attrs({
  activeOpacity: 0.6,
})`
  height: 50px;
  justify-content: center;
  align-items: center;
  background: ${colors.primary};
  padding: ${metrics.padding}px;
  border-radius: ${metrics.borderRadius}px;
`;
