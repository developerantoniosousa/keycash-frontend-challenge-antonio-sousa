import React from 'react';

import {Button} from 'components/Button';
import {ButtonText} from 'components/ButtonText';
import {PropertyItem} from 'global/modules/property/types';

import {
  Container,
  Image,
  Content,
  InfoContainer,
  Price,
  Address,
} from './styles';
import useBrazilianCurrency from 'hooks/useBrazilianCurrency';

export interface PropertyProps {
  readonly data: PropertyItem;
  readonly onPress: (property: PropertyItem) => void;
}

const View = ({data, onPress}: PropertyProps) => {
  const formattedPrice = useBrazilianCurrency(data.price);

  return (
    <Container>
      <Image source={{uri: data.images[0]}} />
      <Content>
        <InfoContainer>
          <Price>{formattedPrice}</Price>
          <Address>{data.address.formattedAddress}</Address>
        </InfoContainer>
        <Button onPress={() => onPress(data)}>
          <ButtonText>Acessar</ButtonText>
        </Button>
      </Content>
    </Container>
  );
};

export const Property = React.memo(View);
