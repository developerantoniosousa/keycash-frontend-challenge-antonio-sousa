import styled from 'styled-components/native';
import {fonts, colors, metrics} from 'styles';

export const Container = styled.View`
  background: ${colors.white};
  margin-bottom: ${metrics.margin}px;
  border-radius: ${metrics.borderRadius}px;
`;

export const Image = styled.Image.attrs({
  resizeMode: 'cover',
})`
  height: 180px;
`;

export const Content = styled.View`
  margin: ${metrics.margin / 1.5}px;
`;

export const InfoContainer = styled.View`
  margin-bottom: ${metrics.margin}px;
`;

export const Price = styled.Text`
  color: ${colors.secondary};
  font-weight: bold;
  font-size: ${fonts.size.large}px;
`;

export const Address = styled.Text`
  color: ${colors.strongGray};
  font-weight: bold;
  font-size: ${fonts.size.medium}px;
`;
