import api from '../utils/api';
import {PropertyItem} from 'global/modules/property/types';

const resource = 'challenge';

class Property {
  static getList(): Promise<{data: PropertyItem[]}> {
    return api.get(resource);
  }
}

export default Property;
