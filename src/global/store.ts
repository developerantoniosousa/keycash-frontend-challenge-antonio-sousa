import {createStore, applyMiddleware, compose, Store} from 'redux';
import ReduxThunk from 'redux-thunk';

import PropertyState from './modules/property/types';
import rootReducers from './rootReducers';
export interface GlobalAppState {
  property: PropertyState;
}

const enhancer = __DEV__
  ? compose(applyMiddleware(ReduxThunk), console.tron.createEnhancer())
  : applyMiddleware(ReduxThunk);

const store: Store<GlobalAppState> = createStore(rootReducers, enhancer);

export {store};
