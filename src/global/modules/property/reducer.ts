import {Reducer} from 'redux';
import PropertyState, {ActionTypes} from './types';

const INITIAL_STATE: PropertyState = {
  list: [],
  isLoading: false,
  isError: false,
};

const PropertyReducer: Reducer<PropertyState> = (
  state = INITIAL_STATE,
  action,
) => {
  switch (action.type) {
    case ActionTypes.LIST_PROPERTIES_REQUEST:
      return {...state, isLoading: true, isError: false};
    case ActionTypes.LIST_PROPERTIES_SUCCESS:
      return {
        ...state,
        list: action.payload.properties,
        isLoading: false,
        isError: false,
      };
    case ActionTypes.LIST_PROPERTIES_FAILURE:
      return {...state, isLoading: false, isError: true};
    default:
      return state;
  }
};

export default PropertyReducer;
