export enum ActionTypes {
  LIST_PROPERTIES_REQUEST = '@property/LIST_PROPERTIES_REQUEST',
  LIST_PROPERTIES_SUCCESS = '@property/LIST_PROPERTIES_SUCCESS',
  LIST_PROPERTIES_FAILURE = '@property/LIST_PROPERTIES_FAILURE',
}

export interface Geopoint {
  lat: number;
  lng: number;
}

export interface PropertyItem {
  id: string;
  address: {
    formattedAddress: string;
    geolocation: Geopoint;
  };
  images: string[];
  price: number;
  bathrooms: number;
  bedrooms: number;
  parkingSpaces: number;
  usableArea: number;
  publish: boolean;
}

export default interface PropertyState {
  list: PropertyItem[];
  isLoading: boolean;
  isError: boolean;
}
