import Property from 'services/Property';
import {ActionTypes, PropertyItem} from 'global/modules/property/types';

export function loadPropertiesRequest() {
  return async (dispatch: Function) => {
    try {
      const response = await Property.getList();

      // remove invalid properties: without address or not published.
      const filteredProperties = response.data.filter(
        property => property.publish && !!property?.address?.formattedAddress,
      );

      const orderedPropertiesByPriceDESC = filteredProperties.sort(
        (a, b) => b.price - a.price,
      );

      dispatch(_loadPropertiesSuccess(orderedPropertiesByPriceDESC));
    } catch {
      dispatch(_loadPropertiesFailure());
    }
  };
}

function _loadPropertiesSuccess(properties: PropertyItem[]) {
  return {
    type: ActionTypes.LIST_PROPERTIES_SUCCESS,
    payload: {
      properties,
    },
  };
}

function _loadPropertiesFailure() {
  return {
    type: ActionTypes.LIST_PROPERTIES_FAILURE,
  };
}
