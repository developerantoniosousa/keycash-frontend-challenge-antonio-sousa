import {combineReducers} from 'redux';

import property from './modules/property/reducer';

export default combineReducers({
  property,
});
