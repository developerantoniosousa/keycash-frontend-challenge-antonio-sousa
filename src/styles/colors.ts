export default {
  primary: '#E20483',
  white: '#FFFFFF',
  secondary: '#0E0E0E',
  lightGray: '#EDEDED',
  strongGray: '#5A5A5A',
};
