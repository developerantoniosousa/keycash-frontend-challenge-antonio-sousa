import styled from 'styled-components/native';
import {metrics} from 'styles';

export const List = styled.FlatList.attrs({
  showsVerticalScrollIndicator: false,
  contentContainerStyle: {
    padding: metrics.padding,
  },
})`
  flex: 1;
`;
