import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import {loadPropertiesRequest} from 'global/modules/property/actions';
import {GlobalAppState} from 'global/store';
import PropertyState from 'global/modules/property/types';
import {PropertyListView} from './PropertyListView';
export function PropertyList() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadPropertiesRequest());
  }, [dispatch]);

  const property = useSelector<GlobalAppState, PropertyState>(
    state => state.property,
  );

  function handleButtonAccessPress() {}

  return (
    <PropertyListView
      properties={property.list}
      isLoading={property.isLoading}
      isError={property.isError}
      onAccessPress={handleButtonAccessPress}
    />
  );
}

PropertyList.navigationOptions = {
  title: 'Keycash',
};
