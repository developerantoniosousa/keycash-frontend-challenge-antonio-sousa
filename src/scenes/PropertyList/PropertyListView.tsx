import React from 'react';
import {Text, ActivityIndicator} from 'react-native';

import {PropertyItem} from 'global/modules/property/types';
import {List} from './styles';
import {Property} from 'components/Property';

export interface PropertyListViewProps {
  readonly properties: PropertyItem[];
  readonly isLoading: Boolean;
  readonly isError: Boolean;
  readonly onAccessPress: (property: PropertyItem) => void;
}

const View = ({
  properties,
  isLoading,
  isError,
  onAccessPress,
}: PropertyListViewProps) => {
  function renderProperty({item}: {item: PropertyItem}) {
    return <Property key={item.id} data={item} onPress={onAccessPress} />;
  }

  if (isLoading) {
    return <ActivityIndicator />;
  }

  if (isError) {
    return (
      <Text>
        Falha no carregamento dos imóveis. Tente novamente mais tarde.
      </Text>
    );
  }

  return <List data={properties} renderItem={renderProperty} />;
};

export const PropertyListView = React.memo(View);
